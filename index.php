<?php

// On affiche les erreurs le temps du dev.
error_reporting(0);
ini_set('display_errors', 0);

// On s'assure que php est sur le bon fuseau horaire.
date_default_timezone_set("Europe/Paris");
setlocale(LC_ALL, 'fr-FR');

 // On lance la session
session_start(array('name' => "token",  // Nom du cookie chez le client
    'cookie_lifetime' => "864000",     //  Durée de vie du cookie de session ( 10 jours)
    'gc_maxlifetime' => "864000"));   //   Durée de vie de la session dans le cache PHP ( 10 jours)

// On vérifie juste que PATH_INFO est correctement défini.
if (!isset($_SERVER['PATH_INFO']) // Il est défini ?
            or $_SERVER['PATH_INFO'] == "" // Si c'est vide
            or substr( $_SERVER['PATH_INFO'], 0, 7 ) === "/index.") { // Si ça commence par /index.
    $_SERVER['PATH_INFO'] = "/";
}

// On nettoie les entrées utilisateurs
require_once("modele/utils/cleanInput.php");
if (isset($_POST)) $_POST = cleanArray($_POST);
if (isset($_GET)) $_GET = cleanArray($_GET);

// Import de la connexion à la base de données.
require_once("modele/bdd.php");

$titreHTML = "La Nouvelle Voix"; // Titre de la page
$contenuHTML = ""; // contenu des pages en HTML généré par le site
$cssHTML = ""; // Liens vers feuilles CSS supplémentaires
$bandeauHTML = ""; // Partie dynamique du bandeau
$navbarHTML = ""; // Contenu de la navbar

$HTML = file_get_contents("vue/html/header-footer.html");

// TODO ajouter une interception des exceptions pour pouvoir afficher un "joli" message d'erreur au client.

// Si le client n'est pas connecté.
if (!isset($_SESSION['typeCompte'])) {

	$bandeauHTML = '<div class="btn-group" role="group" aria-label="Boutons de connexion et d\'inscription">
    <a href="/connexion" class="btn btn-primary">Connexion</a>
    <a href="/inscription" class="btn btn-primary">Inscription</a>
</div>';

    switch ($_SERVER['PATH_INFO']) { // Ici rien de très intelligent
        case "/connexion":
        	$bandeauHTML = '<a href="/inscription" class="btn btn-primary">Inscription</a>';
            $contenuHTML .= require_once("controleur/connexion.php");
            break;
	    case "/motdepasseoublie":
		    $contenuHTML .= require_once("controleur/motDePasseOublie.php");
		    break;
        case "/inscription":
        	$bandeauHTML = '<a href="/connexion" class="btn btn-primary">Connexion</a>';
            $contenuHTML .= require_once("controleur/inscription.php");
            break;
	    case "/": // Accueil
		    $contenuHTML .= require_once("controleur/accueil.php");
		    break;
        default: // Accueil
	        header("Location: /"); // Redirection vers la page d'accueil.
            exit();
    }
} else { // Le client est connecté

    if ($_SERVER['PATH_INFO'] == "/deconnexion") { // Si on demande une déconnexion
        session_destroy();
        header("Location: /"); // Redirection vers la page d'accueil.
    }

    // On génère le bandeau dynamique
    if ($_SESSION['typeCompte'] == 'production')
	    $bandeauHTML = '<h3>Compte '.$_SESSION['typeCompte'].'</h3>';
    else
		$bandeauHTML = '<h3>Bonjour '.$_SESSION['prenom'].'</h3> <h6>Compte '.$_SESSION['typeCompte'].'</h6>';

    // On génère la navbar
	$navbarHTML = '    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Afficher les boutons de navigation"><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse" id="navbar"><ul class="navbar-nav mr-auto">';
//    	<li class="nav-item"><a class="nav-link" href="/">Accueil</a></li>';
	switch ($_SESSION['typeCompte']) {
		case 'télespectateur':
			$navbarHTML .= '
    	<li class="nav-item"><a class="nav-link" href="/">Accueil</a></li>
    	<li class="nav-item"><a class="nav-link" href="/voter">Voter</a></li>';
			require_once('modele/bdd/emission.php');
			if (EM_getTypeDerniereEmission() == 'casting' && !EM_isDerniereTerminee()) {
				$navbarHTML .= '<li class="nav-item"><a class="nav-link" href="/casting">S\'inscrire au casting</a></li>';
			}
			break;
		case 'candidat':
			$navbarHTML .= '
    	<li class="nav-item"><a class="nav-link" href="/">Mes prestations</a></li>';
			break;
		case 'production':
			$navbarHTML .= '
    	<li class="nav-item"><a class="nav-link" href="/">Gestion émissions</a></li>
    	<li class="nav-item"><a class="nav-link" href="/comptes">Gestion des comptes</a></li>
    	<li class="nav-item"><a class="nav-link" href="/votes">Votes public</a></li>
    	<li class="nav-item"><a class="nav-link" href="/archives">Archives</a></li>';
			break;
	}
	$navbarHTML .= '
		<li class="nav-item"><a class="nav-link" href="/parametres">Paramètres du compte</a></li>
	</ul>
	<span class="text-inline"><a class="btn btn-danger" href="/deconnexion">Déconnexion</a></span>
	</div>';

    // Début de l'aiguillage long et pénible mais plus intelligent que la section précédente.
    $retourControleur = array();
    $path = "controleur/"; // On prépare le chemin pour inclure le bon controleur
    switch ($_SESSION['typeCompte']) { // On va voir dans le bon dossier

	    case "production":
		    require_once("modele/bdd/production.php");
        case "télespectateur":
        case "candidat":
	        require_once("modele/bdd/utilisateur.php");
            $path .= $_SESSION['typeCompte'];
            break;
    }
    if ($_SERVER['PATH_INFO'] == '/') // Si on demande la page d'accueil c'est spécifique
        $path .= "/accueil.php";
    else
        $path .= $_SERVER['PATH_INFO'] . '.php';

    if (file_exists($path)) { // On check que le controleur existe

        /** @noinspection PhpIncludeInspection */
        $retourControleur = require_once($path); // On traite la page ...

        // ... puis on ajoute le retour aux bons endroits
	    if (isset($retourControleur['titreHTML'])) $titreHTML = $retourControleur['titreHTML'];
	    if (isset($retourControleur['cssHTML'])) $cssHTML = $retourControleur['cssHTML'];
        $contenuHTML .= $retourControleur['contenuHTML'];

    } else { // Le controleur n'existe pas donc redirection accueil
	    header("Location: /"); // Redirection vers la page d'accueil.
    }
}

$HTML=preg_replace('/{#TITRE}/', $titreHTML, $HTML);
$HTML=preg_replace('/{#CSS}/', $cssHTML, $HTML);
$HTML=preg_replace('/{#BANDEAU}/', $bandeauHTML, $HTML);
$HTML=preg_replace('/{#NAVBAR}/', $navbarHTML, $HTML);
$HTML=preg_replace('/{#CONTENU}/', $contenuHTML, $HTML);

print $HTML;