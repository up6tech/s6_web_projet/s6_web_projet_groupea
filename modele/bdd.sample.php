<?php

// Dev en local
/*$host = "localhost";
$port = 3306;
$username = "root";
$password = "";*/

// Environnement de prod
$host = "otomny.fr";
$port = 3309;
$username = "g1a";
$password = "XXXXXXXXXXXXX";

try {
    $GLOBALS['bdd'] = new PDO("mysql:host=".$host.";port=".$port.";dbname=".$username.";charset=utf8mb4",
        $username,
        $password,
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true,
	        PDO::ATTR_EMULATE_PREPARES => true,
            PDO::MYSQL_ATTR_SSL_CA => "https://letsencrypt.org/certs/isrg-root-x1-cross-signed.pem",
            PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => 0));
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
    exit();
}

unset($host, $port, $username, $password);
