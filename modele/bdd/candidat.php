<?php

/**
 * AJOUTER UNE PRESTATION DANS LA TABLE CHANTER :
 * @param int $idUtilisateur
 * @param string $artiste
 * @param string $titre
 * @param int|null $ordrePassage
 * @param string|null $idLien Identifiant de la vidéo Youtube
 * @return bool renvoie vrai si l'operation s'est bien passee
 */
function CAND_insertionChanter(int $idUtilisateur, string $artiste, string $titre, string $idLien = null, int $ordrePassage = null): bool {
	// on prepare la requete SQL
	$req = $GLOBALS['bdd']->prepare('INSERT INTO Chanter (idEmission, idUtilisateur, ordrePassage, chanson, lienYtb, pointsCoachs) VALUES (?, ?, ?, ?, ?, 0);');

	$chanson = $artiste .' - '.  $titre;
	require_once("modele/bdd/emission.php");
	$idEmission = EM_getIdDerniereEmission();
	try {
		$req->execute(array($idEmission, $idUtilisateur, $ordrePassage, $chanson, $idLien));   // on essaie d'executer la requete preparee
		return true;                                                           //  ça a fonctionné on renvoie true
	} catch (PDOException $e) { 										      //   si ça ne fonctionne pas, on retourne false
		var_dump($e->getMessage());
		return false;
	}
}

/**
 * Enregistre l'équipe d'un utilisateur donné
 * @param int $idUtilisateur
 * @param int $noEquipe Doit être entre 1 et 4 inclus
 * @return bool Renvoie vrai si enregistré avec succès. Faux sinon.
 */
function CAND_setEquipe(int $idUtilisateur, int $noEquipe): bool {

	if ($noEquipe < 1 || $noEquipe > 4) return false;
	$req = $GLOBALS['bdd']->prepare('UPDATE Candidat SET Equipe = ? WHERE idUtilisateur = ?;');
	$req->execute(array($noEquipe, $idUtilisateur));
	return $req->rowCount() == 1;
}

/**
 * Récupère l'équipe d'un utilisateur donné
 * @param int $idUtilisateur
 * @return int|null Renvoie le numéro de l'équipe. Null si introuvable
 */
function CAND_getEquipe(int $idUtilisateur): ?int {

	$req = $GLOBALS['bdd']->prepare('SELECT Equipe FROM Candidat WHERE idUtilisateur = ?;');
	$req->execute(array($idUtilisateur));
	$rep = $req->fetchAll();
	if (isset($rep[0])) return $rep[0]['Equipe'];
	else return null;
}

/**
 * Modifie un élément d'une prestation donnée
 * @param int $idUtilisateur
 * @param string $nomChamp
 * @param string $nouvelleValeur
 * @return int Renvoie le nombre de lignes modifiées.
 */
function CAND_editChanter(int $idUtilisateur, string $nomChamp, string $nouvelleValeur): int {

	require_once('modele/bdd/emission.php');
	require_once('modele/utils/cleanInput.php');
	$nomChamp = cleanInput($nomChamp); // Juste au cas où !
	$req = $GLOBALS['bdd']->prepare('UPDATE Chanter SET '.$nomChamp.' = ? WHERE idEmission = ? AND idUtilisateur = ?;');
	$req->execute(array($nouvelleValeur, EM_getIdDerniereEmission(), $idUtilisateur));
	return $req->rowCount();
}

/**
 * Permet de récupérer les participations à une émission
 * @param int $idEmission Id de l'émission en question
 * @param int|null $idCandidat Id d'un candidat spécifique. Si null toutes les participations de l'émission sont retournées
 * @return array|null Résulat. Renvoie null si aucune participation enregistrée pour l'emission donnée
 */
function CAND_getChanter(int $idEmission, int $idCandidat = null): ?array {

	if ($idCandidat) {
		$req = $GLOBALS['bdd']->prepare('SELECT * FROM Chanter WHERE idEmission = ? AND idUtilisateur = ?;');
		$req->execute(array($idEmission, $idCandidat));
	}
	else {
		$req = $GLOBALS['bdd']->prepare('SELECT * FROM Chanter WHERE idEmission = ?;');
		$req->execute(array($idEmission));
	}

	$retour = $req->fetchAll();
	if (isset($retour[0])) return $retour;
	else return null;
}

/**
 * Permet de récupérer les qualifiés pour l'émission suivante
 * @param int $idEmission Id de l'émission en question
 * @param int|null $equipe No de l'équipe.
 * @return array|null Résulat. Renvoie null si aucun qualifié pour l'emission donnée
 */
function CAND_getQualifies(int $idEmission, int $equipe = null): ?array {

	if (!in_array($equipe, array(null, 1, 2, 3, 4))) return null;

	if ($equipe) {
		$req = $GLOBALS['bdd']->prepare('SELECT Ch.idUtilisateur FROM Chanter Ch, Candidat Ca WHERE Ca.idUtilisateur = Ch.idUtilisateur AND Ch.idEmission = ? AND Ch.pointsCoachs > 0 AND Ca.Equipe = ?;');
		$req->execute(array($idEmission, $equipe));
	}
	else {
		$req = $GLOBALS['bdd']->prepare('SELECT Ch.idUtilisateur FROM Chanter Ch, Candidat Ca WHERE Ca.idUtilisateur = Ch.idUtilisateur AND Ch.idEmission = ? AND Ch.pointsCoachs > 0 ORDER BY Ca.Equipe;');
		$req->execute(array($idEmission));
	}
	$retourSQL = $req->fetchAll();

	if (isset($retourSQL[0])) {
		$retour = array();
		foreach ($retourSQL as $val) { // On traite le tableau pour simplifier le traitement plus tard (PDO renvoie un tableau de tableaux)
			$retour[] = (int)$val['idUtilisateur'];
		}
		return $retour;
	}
	else return null;
}

/**
 * Permet de stocker la qualification (ou non) d'un candidat
 * @param int $idEmission Emission en cours
 * @param int $idCandidat Candidat donné
 * @param bool $isQualifie Vrai si qualifié. Faux si éliminé
 */
function CAND_setResultat(int $idEmission, int $idCandidat, bool $isQualifie) {

	$req = $GLOBALS['bdd']->prepare('UPDATE Chanter SET pointsCoachs = ? WHERE idEmission = ? AND idUtilisateur = ?;');
	$req->execute(array($isQualifie ? 1 : -1, $idEmission, $idCandidat));
}

function CAND_getInfosPerformances(int $idUtilisateur){
    $req = $GLOBALS['bdd']->prepare('SELECT E.type, C.lienYtb FROM Emission E, Chanter C WHERE C.idEmission = E.idEmission AND C.idUtilisateur = ?;');
    $req->execute(array($idUtilisateur));
    $rep = $req->fetchAll();
    if (isset($rep[0])) {
        return $rep;
    } else return null;
}

/**
 * Permet de récupérer les informations du prochain passage sur l'émission courante.
 * @return array|null Renvoie le(s) premier(s) candidat(s) qui doit(vent) passer selon l'ordre de passage pour l'émission courante. Null s'il n'y en as pas.
 */
function CAND_conduiteGetSuivant(): ?array {
	require_once('modele/bdd/emission.php');
	$req = $GLOBALS['bdd']->prepare(
'SELECT U.civilite, U.nom, U.prenom, Ca.Equipe, Ch.*
FROM Chanter Ch, Candidat Ca, Utilisateur U
WHERE Ca.idUtilisateur = Ch.idUtilisateur
AND U.idUtilisateur = Ca.idUtilisateur
AND Ch.idEmission = ?
AND Ch.ordrePassage = (SELECT MIN(ordrePassage) FROM Chanter WHERE pointsCoachs = 0);');

	$req->execute(array(EM_getIdDerniereEmission()));
	$rep = $req->fetchAll();
	if (isset($rep[0])) {
		if (count($rep) == 1) return $rep[0];
		else return $rep;
	}
	else return null;
}
