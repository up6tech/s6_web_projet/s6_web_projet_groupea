<?php

/**
 * Insérer le téléspectateur dans la table Candidat
 * @param int $idTelespect id du téléspectateur qui va devenir un candidat
 * @return bool renvoie vrai si l'insertion s'est bien passée
 */
function TELESPECT_insertionCandidat(int $idTelespect): bool {
	$req = $GLOBALS['bdd']->prepare('INSERT INTO `Candidat` (`idUtilisateur`, `Equipe`) VALUES (?, NULL);');
	try {
		$req->execute(array($idTelespect));
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

/**
 * Récupère l'id du dernier vote (de la table Vote)
 * @return int|null renvoie l'id du dernier vote si l'opération s'est bien passée
 */
function TELESPECT_getIdDernierVote() : ?int {
	$req = $GLOBALS['bdd']->prepare('SELECT MAX(idVote) FROM Vote;');
	$req->execute();
	$rep = $req->fetchAll();
	if (isset($rep[0][0]))
		return $rep[0][0];
	else return null;
}

/**
 * Récupère l'id de l'émission dans laquelle a eu lieu le dernier vote
 * @return int|null renvoie l'id de l'emission du dernier vote si l'opération s'est bien passée
 */
function TELESPECT_getIdEmissionDernierVote() : ?int {
	$req = $GLOBALS['bdd']->prepare('SELECT idEmission FROM Vote WHERE idVote = (SELECT MAX(idVote) FROM Vote);');
	$req->execute();
	$rep = $req->fetchAll();
	if (isset($rep[0]['idEmission']))
		return $rep[0]['idEmission'];
	else return null;
}

/**
 * Insère dans la table ParticiperVote le vote du téléspectateur pour l'idVote le plus récent
 * @param int $idTelespect id du téléspectateur qui vient de voter
 * @param string $reponse numéro de la réponse que le téléspectateur a choisie
 * @return bool renvoie vrai si l'opération s'est bien passée
 */
function TELESPECT_insertionVote(int $idTelespect, string $reponse) : bool {
	// on récupère l'id du dernier Vote
	$idVote = TELESPECT_getIdDernierVote();
	$req = $GLOBALS['bdd']->prepare('INSERT INTO `ParticiperVote` (`idUtilisateur`, `idVote`, `reponse`) VALUES (?, ?, ?);');
	try {
		$req->execute(array($idTelespect, $idVote, $reponse));
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

/**
 * Récupère les réponses proposées lors du dernier Vote de la table Vote
 * @return array|null recupère les reponses du dernier vote si l'opération s'est bien passée
 */
function TELESPECT_getRepDernierVote() : ?array {
	$req = $GLOBALS['bdd']->prepare('SELECT rep1, rep2, rep3, rep4 FROM Vote WHERE idVote = (SELECT MAX(idVote) FROM Vote);');
	$req->execute();
	$rep = $req->fetchAll();
	if (isset($rep[0]['rep1']) && isset($rep[0]['rep2']) && isset($rep[0]['rep3']) && isset($rep[0]['rep4']))
		return $rep[0];
	else return null;
}

/**
 * Récupère la réponse du téléspectateur au vote passé en paramètre
 * @param int $idTelespect id du téléspectateur dont on souhaite récupérer le vote
 * @param int $idVote id du vote pour lequel on souhaite savoir si le téléespectateur a voté
 * @return bool renvoie true si une réponse a été trouvée
 */
function TELESPECT_getVote(int $idTelespect, int $idVote) : bool {
	$req = $GLOBALS['bdd']->prepare('SELECT * FROM ParticiperVote WHERE idUtilisateur = ? AND idVote = ?;');
	$req->execute(array($idTelespect, $idVote));
	$rep = $req->fetchAll();
	if (isset($rep[0]))
		return true;
	else return false;
}

/**
 * Récupère les points obtenus par des candidats qui ont participé à un même vote lors de la dernière émission
 * @param array $idCandidats id des candidats qui ont participé au vote dont on souhaite savoir s'il est toujours ouvet, lors de la dernière émission 
 * @return bool renvoie faux si tous les candidats ont des points attribués
 */
function TELESPECT_isVoteOuvert(array $idCandidats) : bool {
	for ($i=0 ; $i < 4 ; $i++) {
		$req = $GLOBALS['bdd']->prepare('SELECT `pointsCoachs` FROM `Chanter` WHERE `idUtilisateur`= ? AND `idEmission` = (SELECT MAX(`idEmission`) FROM `Chanter`);');
		$req->execute(array($idCandidats[$i]));
		$rep = $req->fetchAll();
		if (!isset($rep[0][0]))
			return true;
	}
	return false;
}
