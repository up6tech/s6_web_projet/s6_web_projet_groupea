<?php

/**
 * Liste des différentes phases du concours
 */
const PHASES_CONCOURS = array('inter-saison', 'casting', 'auditions', 'battles', 'ko', 'demi-finale', 'finale');

/**
 * @return bool Renvoie vrai si la dernière émission de la BDD est terminée. Faux sinon.
 * Un retour a Faux indique soit une émission en cours soit une planifiée mais pas encore démarrée.
 */
function EM_isDerniereTerminee(): bool {

	// Si il n'existe aucun passage de prévu pour la dernière émission c'est forcément faux.
	$req = $GLOBALS['bdd']->prepare('SELECT MAX(idEmission) as id FROM Chanter;');
	$req->execute();
	if ($req->fetchAll()[0]['id'] != EM_getIdDerniereEmission()) return false;

	if (EM_getTypeDerniereEmission() == 'casting') {
		$req = $GLOBALS['bdd']->prepare('SELECT COUNT(C.idUtilisateur) AS nbr FROM Emission E, Chanter C WHERE E.idEmission = C.idEmission AND E.idEmission = (SELECT MAX(idEmission) FROM Emission) AND C.pointsCoachs > 0;');
		$req->execute();
		$rep = $req->fetchAll();
		return $rep[0]['nbr'] > '0';
	} else {
		$req = $GLOBALS['bdd']->prepare('SELECT COUNT(C.idUtilisateur) AS nbr FROM Emission E, Chanter C WHERE E.idEmission = C.idEmission AND E.idEmission = (SELECT MAX(idEmission) FROM Emission) AND C.pointsCoachs = 0;');
		$req->execute();
		$rep = $req->fetchAll();
		return $rep[0]['nbr'] == '0';
	}

}

/**
 * Permet de déterminer le type de la dernière émission enregistrée dans la BDD
 * @return string Type de l'émission.
 */
function EM_getTypeDerniereEmission(): string {
	$req = $GLOBALS['bdd']->prepare('SELECT type FROM Emission ORDER BY idEmission DESC LIMIT 1;');
	$req->execute();
	$rep = $req->fetchAll();
	if (isset($rep[0]['type']))
		return $rep[0]['type'];
	else return 'inter-saison'; // Pas d'entrée dans la BDD donc on commence au tout début
}

/**
 * Permet de déterminer le type de la prochaine émission
 * @param string|null $typeCourant FACULTATIF. Permet de donner un type spécifique. Si null la dernière émission de la BDD est utilisée
 * @return string|null Type de la prochaine émission. Renvoie null si l'argument donné n'est pas un type correct.
 */
function EM_getTypeProchaineEmission(string $typeCourant = null): ?string {

	if ($typeCourant == null) $typeCourant = EM_getTypeDerniereEmission();
	elseif (!in_array ( $typeCourant, PHASES_CONCOURS, true)) return null;

	$key = array_search($typeCourant, PHASES_CONCOURS, true);
	if (is_int($key)) return PHASES_CONCOURS[($key + 1) % count(PHASES_CONCOURS)];
	else return null;
}

/**
 * RECUPERER L'ID DE LA DERNIERE EMISSION :
 * @return int|null renvoie idEmission si l'operation s'est bien passee, renvoie null sinon
 */
function EM_getIdDerniereEmission(): ?int {
	// on prepare la requete SQL
	$req = $GLOBALS['bdd']->prepare("SELECT idEmission FROM Emission ORDER BY idEmission DESC LIMIT 1;");
	$req->execute(array());						// on execute la requete SQL preparee
	$rep = $req->fetchAll();					// on recupere le resultat de la requete dans un array
	if (isset($rep[0]['idEmission'])) {			// si une valeur a ete trouvee, on la retourne
		return $rep[0]['idEmission'];
	} else return null;    						// sinon on retourne null
}

/**
 * RECUPERER L'ID DE L'AVANT DERNIERE EMISSION :
 * @return int|null renvoie idEmission si l'operation s'est bien passee, renvoie null sinon
 */
function EM_getIdAvantDerniereEmission(): ?int {
	// on prepare la requete SQL
	$req = $GLOBALS['bdd']->query('WITH deuxDernier AS (SELECT idEmission FROM Emission ORDER BY idEmission DESC LIMIT 2) SELECT MIN(idEmission) FROM deuxDernier');
	$rep = $req->fetchAll();					// on recupere le resultat de la requete dans un array
	if (isset($rep[0][0])) {			// si une valeur a ete trouvee, on la retourne
		return $rep[0][0];
	} else return null;    						// sinon on retourne null
}

/**
 * Permet de déterminer si la dernière émission de la BDD est en cours.
 * @return bool Vrai si l'émission est commencée. Faux sinon.
 */
function EM_isEmissionEnCours(): bool {

	if (EM_isDerniereTerminee()) return false;
	elseif (EM_isPlanificationEnCours()) return false;

	// Phase entre planification faite et émission terminée
	else {

		$req = $GLOBALS['bdd']->prepare('SELECT COUNT(C.idUtilisateur) AS nbr FROM Chanter C WHERE C.idEmission = ? AND C.pointsCoachs = 0;');
		$req->execute(array(EM_getIdDerniereEmission()));
		$nbrPassagesRestants = (int)$req->fetchAll()[0]['nbr'];

		$req = $GLOBALS['bdd']->prepare('SELECT COUNT(C.idUtilisateur) AS nbr FROM Chanter C WHERE C.idEmission = ?;');
		$req->execute(array(EM_getIdDerniereEmission()));
		$nbrPassagesPrevus = (int)$req->fetchAll()[0]['nbr'];

		if ($nbrPassagesRestants == $nbrPassagesPrevus) {
			// On vérifie le type de compte comme sécurité supplémentaire
			if (isset($_SESSION['typeCompte']) && $_SESSION['typeCompte'] == 'production' && isset($_POST['startConduite'])) return true;
			else return false; // L'émission n'est pas encore commencée
		}
		else return true;
	}
}

/**
 * Permet de déterminer si la dernière émission de la BDD est en cours de planification.
 * @return bool
 */
function EM_isPlanificationEnCours(): ?bool {

	// On commence par déterminer le nombres de candidats qui doivent participer à la prochaine émission.
	$req = $GLOBALS['bdd']->prepare('SELECT COUNT(C.idUtilisateur) AS nbr FROM Emission E, Chanter C WHERE E.idEmission = C.idEmission AND E.idEmission = ? AND C.pointsCoachs > 0;');
	$req->execute(array(EM_getIdAvantDerniereEmission()));
	$nbrCandidatsQualifies = (int)$req->fetchAll()[0]['nbr'];
	// TODO Gérer la pahse finale du concours et l'inter-saison.

	// On compte le nombre de passages planifiés sur la prochaine émission.
	$req = $GLOBALS['bdd']->prepare('SELECT COUNT(idUtilisateur) AS nbr FROM Chanter WHERE idEmission = ?;');
	$req->execute(array(EM_getIdDerniereEmission()));
	$nbrCandidatsPlanifies = (int)$req->fetchAll()[0]['nbr'];

	if ($nbrCandidatsPlanifies < $nbrCandidatsQualifies) return true;

	// On vérifie le type de compte comme sécurité supplémentaire
	if (isset($_SESSION['typeCompte']) && $_SESSION['typeCompte'] == 'production' && isset($_POST['startPlanification'])) return true;
	return false;
}

/**
 * @return int Nombre de candidats qualifiés lors de la dernière émission
 */
function EM_getNbrCandidatsEnLice(): int {
	$req = $GLOBALS['bdd']->prepare('SELECT COUNT(C.idUtilisateur) AS nbr FROM Chanter C WHERE C.idEmission = ? AND C.pointsCoachs >= 0;');
	$req->execute(array(EM_getIdDerniereEmission()));
	return (int)$req->fetchAll()[0]['nbr'];
}

/**
 * Crée une émission dans la BDD
 * @param string $type Type de l'émission (cf. $PHASES_CONCOURS)
 * @param string $date
 * @param string $heure
 * @return bool Renvoie vrai si réussi. Faux sinon.
 */
function EM_creerEmission(string $type, string $date, string $heure): bool {

	if (!in_array ( $type, PHASES_CONCOURS, true)) return false;

	// On récupère le num de saison en cours
	$saison = $GLOBALS['bdd']->query('SELECT MAX(saison) FROM Emission;')->fetch()[0];
	if ($type == 'casting') $saison += 1; // Si c'est une nouvelle on ajoute 1

	$req = $GLOBALS['bdd']->prepare('INSERT INTO Emission (type, saison, dateEmission, heure) VALUES (?, ?, ?, ?);');
	try {
		$req->execute(array($type, $saison, $date, $heure));
	} catch (PDOException $e) {
		return false;
	}

	return true;
}

function EM_getInfosProchaineEmission(int $idEmission): ?array{
    $req = $GLOBALS['bdd']->prepare('SELECT dateEmission as date, heure FROM Emission WHERE idEmission = ?');
    $req->execute(array($idEmission));
    $rep = $req->fetchALL();
	if (isset($rep[0])) return $rep[0];
	else return null;
}

function EM_getPerformancesEmission(int $idEmission){
    $req = $GLOBALS['bdd']->prepare('SELECT E.type, C.lienYtb, C.chanson, U.nom, U.prenom FROM Chanter C, Utilisateur U, Emission E WHERE C.idEmission = ? and E.idEmission = C.idEmission and U.idUtilisateur = C.idUtilisateur');
    $req->execute(array($idEmission));
    $rep = $req->fetchALL();
    if (isset($rep[0]['type']) and isset($rep[0]['lienYtb'])) {
        return $rep;
    }else return null;
}