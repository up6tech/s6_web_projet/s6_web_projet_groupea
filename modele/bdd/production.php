<?php

require_once("modele/bdd/emission.php");             // inclusion du fichier modele/emission.php : EM_getIdDerniereEmission()
require_once("modele/bdd/telespectateur.php"); 

/*
 * Modele du compte production
 */

/**
 * Vérifie que le mot de passe donné correspond à celui enregistré
 * @param array $form mot de passe à tester (en clair)
 * @return bool Retourne true si le mot de passe est correct
 */
function PROD_checkPassword(array $form): bool {

    $req = $GLOBALS['bdd']->prepare('SELECT password FROM Production WHERE username = ?;');
    $req->execute(array($form['mail']));
    $rep = $req->fetchAll();

    if (count($rep) < 1) return false;
    else return password_verify($form['password'], $rep[0]['password']);
}

/**
 * Récupère le mail de la production
 * @return string|null Retourne le mail ou null si la table est vide.
 */
function PROD_getMail(): ?string {

	$req = $GLOBALS['bdd']->prepare('SELECT username FROM Production LIMIT 1;');
	$req->execute(array());
	$rep = $req->fetchAll();

	if (isset($rep[0]['username'])) return $rep[0]['username'];
	else return null;
}

/**
 * Récupère les id des candidats demi-finalistes (ceux qui ont eu le plus de points aux battles)
 * @param int $Equipe numéro de l'équipe (1, 2, 3 ou 4) dont on veut recupérer les id des candidats
 * @return array|null Retourne les id des candidats de l'équipe dans un array si l'opération s'est bien passée
 */
function PROD_getIdDemiFinalistes(int $Equipe) : ?array {
	// on recupère l'id de l'émission précédente (Battles)
	$idKo = EM_getIdDerniereEmission()-1;
	$req = $GLOBALS['bdd']->prepare('SELECT Chanter.idUtilisateur FROM Chanter, Candidat WHERE Chanter.idEmission = ? AND Chanter.idUtilisateur = Candidat.idUtilisateur AND Candidat.Equipe = ? ORDER BY Chanter.pointsCoachs DESC LIMIT 4;');
	$req->execute(array($idKo, $Equipe));
	$rep = $req->fetchAll();
	if (isset($rep[0]['idUtilisateur']) && isset($rep[1]['idUtilisateur']) && isset($rep[2]['idUtilisateur']) && isset($rep[3]['idUtilisateur'])) {
		$result = array ($rep[0]['idUtilisateur'], $rep[1]['idUtilisateur'], $rep[2]['idUtilisateur'], $rep[3]['idUtilisateur']);
		return $result;
	}
	else return null;
}

/**
 * Récupère les id des candidats finalistes (ceux qui ont eu le plus de points aux demi-finales)
 * @return array|null Retourne les id des candidats finalistes dans un array si l'opération s'est bien passée
 */
function PROD_getIdFinalistes() : ?array {
	// on recupère l'id de l'émission précédente (demi-finale)
	$idDemiFinale = EM_getIdDerniereEmission()-1;
	$req = $GLOBALS['bdd']->prepare('SELECT Chanter.idUtilisateur FROM Chanter, Candidat WHERE Chanter.idEmission = ? AND Chanter.idUtilisateur = Candidat.idUtilisateur ORDER BY Chanter.pointsCoachs DESC LIMIT 4;');
    $req->execute(array($idDemiFinale));
	$rep = $req->fetchAll();
	for ($i=0; $i<4; $i++) {
		if (!isset($rep[$i]['idUtilisateur']))
			return null;
	}
    $result = array ($rep[0]['idUtilisateur'], $rep[1]['idUtilisateur'], $rep[2]['idUtilisateur'], $rep[3]['idUtilisateur']);
	return $result;
}

/**
 * Récupère le prénom et le nom d'un candidat à partir de son id
 * @param int $idUtilisateur id de l'utilisateur dont on veut récupérer le nom et le prénom
 * @return string|null Retourne le prénom-nom ou null si le resultat est vide
 */
function PROD_getPrenomNomById(int $idCandidat): ?string {
	$req = $GLOBALS['bdd']->prepare('SELECT prenom, nom FROM Utilisateur WHERE idUtilisateur = ?;');
	$req->execute(array($idCandidat));
	$rep = $req->fetchAll();
	if (isset($rep[0]['prenom']) && isset($rep[0]['nom']))
		return $rep[0]['prenom'].' '.$rep[0]['nom'];
	else return null;
}

/**
 * Récupère la chanson d'un candidat à partir de son id et de l'id de l'émission
 * @param int $idUtilisateur id du candidat dont on souhaite connaitre la chanson
 * @param int $idEmission id de l'émission pour laquelle on souhaite connaitre la chanson du candidat
 * @return string|null Retourne la chanson ou null si le resultat est vide
 */
function PROD_getChansonByIdAndEmission(int $idCandidat, int $idEmission): ?string {
	$req = $GLOBALS['bdd']->prepare('SELECT chanson FROM Chanter WHERE idUtilisateur = ? AND idEmission = ? ;');
	$req->execute(array($idCandidat, $idEmission));
	$rep = $req->fetchAll();
	if (isset($rep[0]['chanson']))
		return $rep[0]['chanson'];
	else return null;
}

/**
 * Insère un nouveau vote dans la table Vote (=créée un nouveau vote)
 * @param array $idCandidat tableau contenant les id des candidats concernés par le vote
 * @return bool renvoie vrai si l'insertion s'est bien passée
 */
function PROD_creerVote(array $idCandidats): bool {
	$req = $GLOBALS['bdd']->prepare('INSERT INTO `Vote` (`idVote`, `rep1`, `rep2`, `rep3`, `rep4`, `idEmission`) VALUES (NULL, ?, ?, ?, ?, ?);');
	try {
		$req->execute(array($idCandidats[0], $idCandidats[1], $idCandidats[2], $idCandidats[3], EM_getIdDerniereEmission()));
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

/**
 * Insère dans Chanter les points obtenus par les candidats concernés par le précédent vote
 * @param array $notes tableau contenant les notes finales obtenues par chacun des candidats concernés par le précédent vote
 * @param array $idCandidats tableau contenant les id des candidats concernés par le précédent vote
 * @return bool renvoie vrai si l'insertion s'est bien passée
 */
function PROD_insererNotes(array $notes, array $idCandidats): bool {
	for ($i=0 ; $i<count($idCandidats) ; $i++) {
		$req = $GLOBALS['bdd']->prepare('UPDATE `Chanter` SET `pointsCoachs` = ? WHERE `idUtilisateur` = ? AND `idEmission` IN (SELECT MAX(`idEmission`) FROM `Emission`);');
		try {
			$req->execute(array($notes[$i], $idCandidats[$i]));
		} catch (PDOException $e) {
			return false;
		}
	}
	return true;
}

/**
 * Récupère, pour chaque réponse proposée, le nombre de votes des téléspectateurs pour le dernier vote de la table Vote
 * @return array|null renvoie les nombres de votes pour chaque réponse si l'opération s'est bien passée
 */
function PROD_recupererVotesTelespect() : ?array {
	$req = $GLOBALS['bdd']->prepare('SELECT COUNT(*)FROM `ParticiperVote` WHERE `idVote` = (SELECT MAX(`idVote`) FROM `Vote`) GROUP BY `reponse` ORDER BY `reponse`;');
	$req->execute();
	$rep = $req->fetchAll();
	if (isset($rep))
		return $rep;
	else return null;
}


/**
 * Récupère le nombre de votes en tout pour le dernier vote de la table Vote
 * @return int|null renvoie le nombre total de votes si l'opération s'est bien passée
 */
function PROD_recupererNbTotalVotes() : ?int {
	$req = $GLOBALS['bdd']->prepare('SELECT COUNT(*) FROM `ParticiperVote` WHERE `idVote` = (SELECT MAX(`idVote`) FROM `Vote`);');
	$req->execute();
	$rep = $req->fetchAll();
	if (isset($rep[0][0]))
		return $rep[0][0];
	else return null;
}

/**
 * Récupère les scores finaux des candidats donnés en paramètre
 * @return array|null renvoie les scores finaux des candidats
 */
function PROD_recupererScores(array $idUtilisateur) : ?array {
	$scores = array();
	for ($i=0 ; $i < count($idUtilisateur) ; $i++) {
		$req = $GLOBALS['bdd']->prepare('SELECT pointsCoachs FROM Chanter WHERE idUtilisateur = ? AND idEmission = (SELECT MAX(idEmission) FROM Chanter);');
		$req->execute(array($idUtilisateur[$i]));
		$rep = $req->fetchAll();
		if (!isset($rep[0][0]))
			return null;
		$scores[$i] = $rep[0][0];
	}
	return $scores;
}

/**
 * Modifie un élément du compteProduction
 * @param string $nomChamp
 * @param string $nouvelleValeur
 * @return int Renvoie le nombre de lignes modifiées.
 */
function PROD_edit(string $nomChamp, string $nouvelleValeur): int {
    require_once('modele/utils/cleanInput.php');
    $nomChamp = cleanInput($nomChamp); // Juste au cas où !
    $req = $GLOBALS['bdd']->prepare('UPDATE Production SET ' . $nomChamp . ' = ?;');
    $req->execute(array($nouvelleValeur));
    return $req->rowCount();
}

/**
 * Récupère les votes qui ont les mêmes réponses
 * @return bool renvoie vrai si un vote identique a été trouvé
 */
function PROD_isVoteExistant() : bool {
	if (TELESPECT_getRepDernierVote() == NULL) return false;
	$req = $GLOBALS['bdd']->prepare('SELECT `idVote` FROM `Vote` WHERE `rep1`=? AND `rep2`=? AND `rep3`=? AND `rep4`=? AND `idEmission`=?;');
	$req->execute(array(TELESPECT_getRepDernierVote()[0],
						TELESPECT_getRepDernierVote()[1],
						TELESPECT_getRepDernierVote()[2],
						TELESPECT_getRepDernierVote()[3],
						EM_getIdDerniereEmission()));
	$rep = $req->fetchAll();
	if (isset($rep[0][0]))
		return true;
	return false;
}
