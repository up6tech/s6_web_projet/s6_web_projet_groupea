<?php


/*
 * ON REGROUPE LES PARTIES COMMUNES A TOUS LES TYPES DE COMPTES
 */

/**
 * Hashe un mot de passe donné.
 * @param string $password Mot de passe (en clair)
 * @return string Retourne le mot de passe hashé
 */
function USER_hashPassword(string $password): string {
    return password_hash($password, PASSWORD_ARGON2ID);
}

/**
 * Ajoute un compte utilisateur dans la base de données
 * @param string $prenom
 * @param string $nom
 * @param string $civilite
 * @param string $tel
 * @param string $mail
 * @param string $adresse
 * @param string $password En clair !!
 * @param string $dateNaissance
 * @param string $question
 * @param string $reponse
 * @return bool Renvoie vrai si l'opération s'est bien passée.
 */
function USER_createAccount(string $prenom, string $nom, string $civilite,
                            string $tel, string $mail, string $adresse,
                            string $password, string $dateNaissance, string $question, string $reponse): bool {

    $req = $GLOBALS['bdd']->prepare('INSERT INTO `Utilisateur` (`civilite`, `nom`, `prenom`, `mdp`, `mail`, `tel`, `adresse`, `dateNaissance`, `questSecrete`, `repSecrete`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);');

	try {
		$req->execute(array($civilite, $nom, $prenom,
			USER_hashPassword($password), $mail, $tel, $adresse,
			$dateNaissance, $question, $reponse ));
	} catch (PDOException $e) {
		return false;
	}
    return true;
}

/**
 * Vérifie que le mot de passe donné correspond à celui enregistré
 * @param string $mail
 * @param string $password mot de passe à tester (en clair)
 * @return bool Retourne true si le mot de passe est correct
 */
function USER_checkPassword(string $mail, string $password): bool {

    $req = $GLOBALS['bdd']->prepare('SELECT mdp FROM Utilisateur WHERE mail = ?;');
    $req->execute(array($mail));
	$rep = $req->fetchAll();
	if (isset($rep[0]['mdp']))
		return password_verify($password, $rep[0]['mdp']);
	else return false;
}

/**
 * Récupère l'id d'un utilisateur à partir de son mail
 * @param string $mail
 * @return string|null Id de l'utilisateur si il existe null sinon
 */
function USER_getIdByMail(string $mail): ?string {

	$req = $GLOBALS['bdd']->prepare('SELECT idUtilisateur FROM Utilisateur WHERE mail = ?;');
	$req->execute(array($mail));
	$rep = $req->fetchAll();
	if (isset($rep[0]['idUtilisateur']))
		return $rep[0]['idUtilisateur'];
	else return null;
}

/**
 * Récupère toutes les infos d'un user à partir de son identifiant
 * @param int $id Identifiant dans la table Utilisateurs
 * @return array|null Tableau associatif contenant la ligne de l'utilisateur si elle existe. Renvoie null si l'utilisateur est introuvable
 */
function USER_getInfosById(int $id): ?array {

    $req = $GLOBALS['bdd']->prepare('SELECT * FROM Utilisateur WHERE idUtilisateur = ?;');
    $req->execute(array($id));
    $rep = $req->fetchAll();
    if (isset($rep[0]))
    	return $rep[0];
    else return null;
}

/**
 * Récupère toutes les infos d'un user à partir de son mail
 * @param string $mail Mail dans la table Utilisateurs
 * @return array|null Tableau associatif contenant la ligne de l'utilisateur si elle existe. Renvoie null si l'utilisateur est introuvable
 */
function USER_getInfosByMail(string $mail): ?array {

    $req = $GLOBALS['bdd']->prepare('SELECT * FROM Utilisateur WHERE mail = ?;');
    $req->execute(array($mail));
	$rep = $req->fetchAll();
	if (isset($rep[0]))
		return $rep[0];
	else return null;
}

/**
 * Détermine si le compte pointé est un compte candidat ou non.
 * @param int $idCandidat Identifiant du candidat
 * @return bool Retourne vrai si c'est le cas. Faux sinon. Si l'utilisateur est introuvable renvoie faux.
 */
function USER_isCandidat(int $idCandidat): bool {

    $req = $GLOBALS['bdd']->prepare('SELECT count(idUtilisateur) AS Count FROM Candidat WHERE idUtilisateur = ?;');
    $req->execute(array($idCandidat));
    return $req->fetchAll()[0]['Count'] === "1";
}

/**
 * Modifie un élément d'un utilisateur donné
 * @param string $idUser
 * @param string $nomChamp
 * @param string $nouvelleValeur
 * @return int Renvoie le nombre de lignes modifiées.
 */
function USER_edit(string $idUser, string $nomChamp, string $nouvelleValeur): int {

	require_once('modele/utils/cleanInput.php');
	$nomChamp = cleanInput($nomChamp); // Juste au cas où !
	$req = $GLOBALS['bdd']->prepare('UPDATE Utilisateur SET '.$nomChamp.' = ? WHERE idUtilisateur = ?;');
	$req->execute(array($nouvelleValeur, $idUser));
	return $req->rowCount();
}