<?php

/**
 * Permet de vérifier si le lien youtube à un format valide
 * @param string $lien Lien youtube
 * @return bool Renvoie vrai si le lien est dans un format correct
 */
function YT_isValid(string $lien): bool {

    return preg_match("#(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?#", $lien) === 1;
}

/**
 * Permet d'extraire l'id de la vidéo d'un lien youtube
 * @param string $lien Lien youtube
 * @return string|null Renvoie l'id si le lien est valide. Null sinon.
 */
function YT_getID(string $lien): ?string {

    if (preg_match("#(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?#", $lien, $matches)) {
        return $matches[1];
    } else {
        return null;
    }
}

/**
 * Permet de générer le code HTML pour incorporer une vidéo youtube dans une page
 * @param string $idVideo Id de la vidéo en question
 * @return string Code HTML correspondant.
 */
function YT_getHTML(string $idVideo): string {

    return '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/'.$idVideo.'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
}

/*
// Ne fonctionne pas il semble y avoir des restrictions d'utilisation de l'API Youtube. Flemme de demander un token ect donc pour le moment on vérifie pas
function YT_videoExists($videoID) {

    $theURL = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=" . $videoID."&format=json";
    $headers = get_headers($theURL);
    return (substr($headers[0], 9, 3) !== "404");
}*/