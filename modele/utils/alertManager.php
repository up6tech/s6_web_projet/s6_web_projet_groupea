<?php

/**
 * Affiche un message d'alerte à l'endroit prévu pour une chaine donnée
 * @param string $chaine Chaine de caractères
 * @param string $message Message d'alerte à afficher
 * @param string $typeAlerte cf. Types disponibles https://getbootstrap.com/docs/4.6/components/alerts/
 * @return string Contenu de la chaine avec l'alerte insérée.
 */
function ALERT_show_s(string $chaine, string $message, string $typeAlerte): string {

	$contenu = '<div class="alert alert-'.$typeAlerte.'" role="alert">
    '.$message.'
</div>';

	return preg_replace('/<!--ALERT-->/', $contenu, $chaine, 1);
}

/**
 * Affiche un message d'alerte à l'endroit prévu pour un fichier HTML donné
 * @param string $cheminFichierSource Chemin vers le fichier HTML
 * @param string $message Message d'alerte à afficher
 * @param string $typeAlerte cf. Types disponibles https://getbootstrap.com/docs/4.6/components/alerts/
 * @return string Contenu du fichier HTML avec l'alerte insérée.
 */
function ALERT_show(string $cheminFichierSource, string $message, string $typeAlerte): string {

	if (pathinfo($cheminFichierSource, PATHINFO_EXTENSION) == 'php')
		return ALERT_show_s(require($cheminFichierSource), $message, $typeAlerte);
	else return ALERT_show_s(file_get_contents($cheminFichierSource), $message, $typeAlerte);
}