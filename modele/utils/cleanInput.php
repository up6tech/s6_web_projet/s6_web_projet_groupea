<?php


/**
 * Nettoie les entrées utilisateurs
 * @param string $data Chaine à nettoyer
 * @return string Chaine sécurisée
 */
function cleanInput(string $data): string {

    return htmlspecialchars(stripslashes(stripcslashes(trim($data))));
}

/**
 * Nettoie les tableaux avec cleanInput
 * @param array $data Tableau a nettoyer
 * @return array Tableau sécurisé
 */
function cleanArray(array $data): array {

    foreach ($data as &$value) {
        $value = cleanInput($value);
    }
    return $data;
}