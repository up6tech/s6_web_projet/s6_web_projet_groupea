<?php

$corp = '
<h1>Casting</h1><br>

<form action="/" method="POST">
  {#CHAMPS}
  <br>
  <div class="container text-center">
    <button class="btn btn-primary btn-lg" name="startConduite" type="submit">Valider le casting</button>
  </div>
</form>';

$champ = '<div class="form-row">
    <div class="form-group col-md-6">
      {#YOUTUBE}
    </div>
    <div class="form-group col-md-6 align-self-center">
      <h3>{#NOM}</h3>
      <h4>{#CHANSON}</h4>
      <div class="row align-items-center justify-content-center">
        <label class="col-md-auto col-form-label col-form-label-lg" for="{#ID}">Qualifier</label>
        <div class="col-md-auto">
          <input class="form-control" id="{#ID}" name="{#ID}" value="{#ID}" type="checkbox">
        </div>
      </div>
    </div>
  </div>';

return array('corp' => $corp, 'champ' => $champ);