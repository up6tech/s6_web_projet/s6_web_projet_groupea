<?php

return '<h1>Créer un compte</h1><br>

<form action="/inscription" method="POST">
    <h2>Informations personnelles</h2><br>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="prenom">Prénom:</label>
            <input class="form-control" maxlength="50" autofocus required id="prenom" name="prenom" type="string"/>
        </div>
        <div class="form-group col-md-6">
            <label for="nom">Nom:</label>
            <input class="form-control" id="nom" maxlength="50" required name="nom" type="string">
        </div>
    </div>
    <div class="form-group">
        <label for="dateDeNaissance">Date de naissance</label>
        <input class="form-control" id="dateDeNaissance" required name="dateNaissance" max="'.date('Y-m-d').'" type="date">
    </div>
    <div class="form-group">
        <label>Je me définis comme:</label><br>
        <div class="form-check form-check-inline">
            <input class="form-check-input" id="homme" required name="sexe" type="radio" value="H">
            <label class="form-check-label" for="homme">Homme</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" id="femme" required name="sexe" type="radio" value="F">
            <label class="form-check-label" for="femme">Femme</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" id="nonBinaire" required name="sexe" type="radio" value="N">
            <label class="form-check-label" for="nonBinaire">Non-Binaire</label>
        </div>
    </div>

    <br>
    <h3>Mes coordonnees</h3>
    <div class="form-group">
        <label for="numTel">Téléphone</label>
        <input class="form-control" id="numTel" minlength="10" maxlength="10" pattern="[0-9]{10}" required name="tel" type="string">
    </div>
    <div class="form-group">
        <label for="mail">Mail</label>
        <input class="form-control" id="mail" maxlength="100" required name="mail" type="email">
    </div>
    <div class="form-group">
        <label for="adresse">Adresse</label>
        <input class="form-control" id="adresse" placeholder="n°, nom de la rue" maxlength="125" required name="adresse" type="string">
    </div>
    <div class="form-row">
        <div class="form-group col-md-10">
            <label for="ville">Ville</label>
            <input class="form-control" id="ville" maxlength="66" required name="ville" type="text">
        </div>
        <div class="form-group col-md-2">
            <label for="codePostal">Code Postal</label>
            <input class="form-control" id="codePostal" minlength="5" maxlength="7" pattern="[0-9]{5,7}"  required name="codePostal" type="text">
        </div>
    </div>

    <br>
    <h3>Mon parcours</h3>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="password">Mot de passe:</label>
            <input class="form-control" id="password" required name="password" type="password"/>
        </div>
        <div class="form-group col-md-6">
            <label for="confPassword">Confirmation: </label>
            <input class="form-control" id="confPassword" onchange="validateConfPassword()" required name="confPassword" type="password">
        </div>
    </div>
    <div class="form-group">
        <label for="questionSecrete">Question secrète: </label>
        <input class="form-control" id="questionSecrete" maxlength="200" required name="questionSecrete" type="string">
    </div>
    <div class="form-group">
        <label for="repQuestionSecrete">Réponse: </label>
        <input class="form-control" id="repQuestionSecrete" maxlength="200" required name="repQuestionSecrete" type="string">
    </div>
    <br>
    <!--ALERT-->
    <div class="container text-center">
        <button class="btn btn-primary btn-lg" type="submit">S\'inscrire</button>
    </div>
    <script type="text/javascript">
    	function validateConfPassword() {
    	  let password = document.getElementById("password")
    	  let confPassword = document.getElementById("confPassword")
    	  if (password.value === confPassword.value) {
    	      confPassword.classList.remove("is-invalid")
    	      confPassword.classList.add("is-valid")
    	  } else {
    	      confPassword.classList.remove("is-valid")
    	      confPassword.classList.add("is-invalid")
    	  }
    	}
	</script>
</form>';
