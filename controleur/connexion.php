<?php

// Envoi de la page simple
if (!isset($_POST['password']) || !isset($_POST['mail']) || !filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL))
    return file_get_contents("vue/html/connexion.html");

else { // Le client viens de répondre au formulaire donc on tente de le connecter.

    require_once("modele/bdd/production.php");
    require_once("modele/bdd/utilisateur.php");

	$_POST['mail'] = substr($_POST['mail'], 0, 100);
    if (PROD_checkPassword($_POST)) {
        $_SESSION['typeCompte'] = 'production';
	    header("Location: /"); // Redirection vers la page d'accueil.
    } else if (USER_checkPassword($_POST['mail'], $_POST['password'])) { // Correctement connecté

        $idCompte = USER_getIdByMail($_POST['mail']);
        $_SESSION['idUtilisateur'] = $idCompte;
        $_SESSION['prenom'] = USER_getInfosById($idCompte)['prenom'];

        if (USER_isCandidat($idCompte))
            $_SESSION['typeCompte'] = "candidat";
        else
            $_SESSION['typeCompte'] = "télespectateur";

        header("Location: /"); // Redirection vers la page d'accueil.
    } else { // Pb d'identifiants

    	require_once('modele/utils/alertManager.php');
    	return ALERT_show('vue/html/connexion.html', 'Identifiants incorrects', 'danger');
    }


}