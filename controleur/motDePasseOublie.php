<?php

require_once("modele/bdd/utilisateur.php");

if (isset($_POST['mail']) && isset($_POST['reponse']) && isset($_POST['password'])) {

	$_POST['mail'] = substr($_POST['mail'], 0, 100);
	$_POST['reponse'] = substr($_POST['reponse'], 0, 200);
	$compte = USER_getInfosByMail($_POST['mail']);
	if ($_POST['reponse'] == $compte['repSecrete']) {

		$retour = USER_edit($compte['idUtilisateur'], 'mdp', USER_hashPassword($_POST['password']));
		if ($retour == 1) header("Location: /connexion");
		else {
			require_once('modele/utils/alertManager.php');
			return ALERT_show('vue/html/motDePasseOublie.html', 'Erreur lors de la modification du mot de passe.', 'danger');
		}

	} else {
		require_once('modele/utils/alertManager.php');
		return ALERT_show('vue/html/motDePasseOublie.html', 'La réponse à la question secrète est incorrecte.', 'danger');
	}
} else return file_get_contents("vue/html/motDePasseOublie.html");