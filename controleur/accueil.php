<?php

require_once('modele/bdd/emission.php');
require_once("modele/utils/youtubeParser.php");

$retour = array();

$retour['contenuHTML'] = '<h1>La Nouvelle Voix</h1><br>';
$retour['titreHTML'] = "Accueil";

$retour['contenuHTML'] .= '<h2>Faites entendre VOTRE voix!</h2><h2>Inscrivez ou connectez vous pour participer aux votes</h2><br>';


if (EM_isEmissionEnCours()) {
    $retour['contenuHTML'] .= '<h3>Allumez votre écran, en direct c\'est les ' . EM_getTypeDerniereEmission().'!</h3><br>';
    /*if (TELESPECT_isVoteOuvert()) {
        $retour['contenuHTML'] .= '<h2>Ne ratez pas votre chance de participation, <a href="/voter">Votez maintenant!</a></h2>';
    }*/
}
else{
    $retour['contenuHTML'] .= '<h3>Ne ratez pas la prochaine émission! C\'est les '.EM_getTypeProchaineEmission().'</h3><br>';
    if (EM_isPlanificationEnCours()){
        $infosEmission = EM_getInfosProchaineEmission(EM_getIdDerniereEmission());
        $retour['contenuHTML'] .= '<h4>'.strftime('%A %e %B %Y', date_create_from_format('Y-m-d', $infosEmission['date'])->getTimestamp()).'</h4>';
    }
}

$vide = true;
for ($i = EM_getIdDerniereEmission(); $i > 0; $i--) {
	$performances = EM_getPerformancesEmission($i);
	if ($performances != null) {
		$vide = false;
		$nbVideos = 0;
		$retour['contenuHTML'] .= '<br><div class="container bordures"><h3>' . strtoupper($performances[0]['type']) . '</h3><br>';
		foreach ($performances as $candidat) {
			if ($candidat['lienYtb']) {
				if ($nbVideos % 3 == 0) {
					if ($nbVideos != 0) $retour['contenuHTML'] .= '</div>';
					$retour['contenuHTML'] .= '<div class=row>';
				}

				$retour['contenuHTML'] .= '<div class="col text-center">' . YT_getHTML($candidat['lienYtb']) . '<br><h5>' . $candidat['prenom'] . ' ' . $candidat['nom'] . '</h5><h6>' . $candidat['chanson'] . '</h6></div>';
				$nbVideos++;
			}
		}
		$retour['contenuHTML'] .= '</div></div>';
	}
}

if ($vide){$retour['contenuHTML'] .= '<h3>Aucun contenu disponible</h3>';}

return $retour['contenuHTML'];
