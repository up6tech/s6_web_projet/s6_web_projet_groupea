<?php

// inclusion des fichiers pour les fonctions utilisées
require_once("modele/bdd/emission.php");
require_once("modele/bdd/telespectateur.php");
require_once("modele/bdd/production.php");

// preparation du $retour pour constituer la page HTML
$retour = array();
$retour['titreHTML'] = "Votes public";
if (EM_getTypeDerniereEmission()=="demi-finale") {
    $h1HTML = 'Demi-Finale';
}
else {
    $h1HTML = 'Finale';
}

// on récupère les réponses du vote
$idCandidats = array();
for ($i=0; $i < 4; $i++) {
    $idCandidats[$i] = TELESPECT_getRepDernierVote()[$i];
    $nomHTML[$i] = PROD_getPrenomNomById(TELESPECT_getRepDernierVote()[$i][0]);
}

// si la dernière émission est la finale mais qu'elle est terminée ou que la dernière émission est la demi-finale mais que le dernier vote est fermé
// ou que le dernière émission n'est ni une finale ni une demi-finale
if (EM_getTypeDerniereEmission()=='finale' && EM_isDerniereTerminee() ||
    EM_getTypeDerniereEmission()=='demi-finale' && !TELESPECT_isVoteOuvert($idCandidats) ||
    EM_getTypeDerniereEmission()!='demi-finale' && EM_getTypeDerniereEmission()!='finale') {
    $retour['contenuHTML'] = file_get_contents("vue/html/candidat/vote_indisponible.html");
    return $retour;
}

// sinon, si le telespectateur a deja voté
else if (TELESPECT_getVote($_SESSION['idUtilisateur'], TELESPECT_getIdDernierVote()) && !isset($_POST['voteTelespectateur'])) {
    $retour['contenuHTML'] = file_get_contents("vue/html/candidat/vote_dejaVote.html");
    return $retour;
}

// si le téléspectateur n'a pas encore voté
else if (!isset($_POST['voteTelespectateur'])) {
    $retour['contenuHTML'] = file_get_contents("vue/html/candidat/votes.html");

    // on effectue l'insertion dans la page HTML
    $retour['contenuHTML']=preg_replace('/{#CANDIDAT1}/', $nomHTML[0], $retour['contenuHTML']);
    $retour['contenuHTML']=preg_replace('/{#CANDIDAT2}/', $nomHTML[1], $retour['contenuHTML']);
    $retour['contenuHTML']=preg_replace('/{#CANDIDAT3}/', $nomHTML[2], $retour['contenuHTML']);
    $retour['contenuHTML']=preg_replace('/{#CANDIDAT4}/', $nomHTML[3], $retour['contenuHTML']);
    return $retour;

}

// sinon
else {
    if (TELESPECT_insertionVote($_SESSION['idUtilisateur'], $_POST['voteTelespectateur'])) {
        $retour['contenuHTML'] = file_get_contents("vue/html/candidat/vote_dejaVote.html");
        return $retour;
    }  else $retour['contenuHTML'] = '<h1>ERREUR</h1>';
}