<?php
// Construction de la page
$retour = array();
$retour['contenuHTML'] = file_get_contents("vue/html/telespectateur/casting.html");
$retour['titreHTML'] = "Casting";

require_once("modele/bdd/emission.php");                 // inclusion du fichier modele/emission.php : EM_isDerniereTerminee()

if (!(EM_getTypeDerniereEmission() == 'casting' && !EM_isDerniereTerminee())) { // On vérifie juste que l'on soit dans une phase casting
	header("Location: /"); // On redirige vers la page d'accueil
	exit(); // On force la sortie pour ne pas exécuter la suite
}

require_once("modele/utils/youtubeParser.php");                       // inclusion du fichier youtubeParser.php : YT_isValid(), YT_getID()
// Envoi de la page simple
if (!isset($_POST['artiste']) || !isset($_POST['titre']) || !isset($_POST['lien']) || !YT_isValid($_POST['lien'])) {
    return $retour;
}
// Le client vient de repondre au formulaire donc on tente d'enregistrer sa candidature
else { // TODO Si on a le temps rechecker les inputs user (cf. inscription)
    require_once("modele/bdd/telespectateur.php");           // inclusion du fichier modele/telespectateur.php : TELESPECT_insertionCandidat(), USER_getIdEmissionByDateAndType(), TELESPECT_insertionChanter()
	require_once("modele/bdd/candidat.php");

	TELESPECT_insertionCandidat($_SESSION['idUtilisateur']);        // on ajoute le telespectateur dans la table Candidat
	CAND_insertionChanter($_SESSION['idUtilisateur'], $_POST['artiste'], $_POST['titre'], YT_getID($_POST['lien']));   // on ajoute une prestation dans la table Chanter
	$_SESSION['typeCompte'] = 'candidat';                           // on change le type de compte (telespectateur --> candidat)
	header("Location: /");                                          // on redirige enfin le nouveau candidat vers sa nouvelle page d'accueil
}
return $retour;