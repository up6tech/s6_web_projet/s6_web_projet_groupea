<?php

require_once("modele/bdd/utilisateur.php");

$html = "";

if (isset($_POST['typeEdit'])) {
    switch ($_POST['typeEdit']) {
        //Si l'utilisateur clique sur le bouton "Modifier vos coordonnees"
        case 'coordonnees':
            //On prend en compte les erreurs de saisie pour le numero de telephone sinon on modifie le champ
            if (!preg_match('/([0-9]){10}/', $_POST['tel'])) {
                require_once('modele/utils/alertManager.php');
                $html = ALERT_show('vue/html/parametres.html', 'Veuillez entrer un numéro valide.', 'danger');
            } else {
                USER_edit($_SESSION['idUtilisateur'], 'tel', $_POST['tel']);
            }

            //On prend en compte les erreurs de saisie pour le mail sinon on modifie le champ
            if (!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
                require_once('modele/utils/alertManager.php');
                if ($html == "") $html = ALERT_show('vue/html/parametres.html', 'Veuillez entrer un mail valide.', 'danger');
                else $html = ALERT_show_s($html, 'Veuillez entrer un mail valide.', 'danger');
            } else {
                USER_edit($_SESSION['idUtilisateur'], 'mail', $_POST['mail']);
            }

            USER_edit($_SESSION['idUtilisateur'], 'adresse', $_POST['adresse']);
            break;

        //Si l'utilisateur clique sur le bouton "Modifier mon mot de passe" (Le controle de saisie s'effectue dans l'html)
        case 'motdepasse':
            if ($_POST['password'] == $_POST['confPassword']) {
                USER_edit($_SESSION['idUtilisateur'], 'mdp', USER_hashPassword($_POST['password']));
            }
            break;

        //Si l'utilisateur clique sur le bouton "Modifier ma question secrète"
        case 'question':
            USER_edit($_SESSION['idUtilisateur'], 'questSecrete', $_POST['questionSecrete']);
            USER_edit($_SESSION['idUtilisateur'], 'repSecrete', $_POST['reponse']);
            break;
    }
}
//On recupere les informations de l'utilisateur actuel afin de lui renvoyer ses informations
$compteUser = USER_getInfosById($_SESSION['idUtilisateur']);

if ($html == "") $html = file_get_contents('vue/html/parametres.html');
$retour['titreHTML'] = "Paramètres du compte";

//Referencement des differents parametres
$html=preg_replace('/{#PRENOM}/', $compteUser['prenom'], $html);
$html=preg_replace('/{#NOM}/', $compteUser['nom'], $html);
$html=preg_replace('/{#DATENAISSANCE}/', $compteUser['dateNaissance'], $html);
$html=preg_replace('/{#TEL}/', $compteUser['tel'], $html);
$html=preg_replace('/{#MAIL}/', $compteUser['mail'], $html);
$html=preg_replace('/{#ADRESSE}/', $compteUser['adresse'], $html);
$html=preg_replace('/{#QUESTION}/', $compteUser['questSecrete'], $html);
$retour['contenuHTML']=preg_replace('/{#TODAY}/', date('Y-m-d'), $html);

return $retour;