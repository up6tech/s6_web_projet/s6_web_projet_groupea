<?php

require_once("modele/bdd/emission.php");

$retour = array();
$retour['contenuHTML'] = file_get_contents('vue/html/production/gestionEmissions/attente.html');
$retour['titreHTML'] = "Accueil production";

$titre = '';
$type = EM_getTypeDerniereEmission();

if (EM_isDerniereTerminee()) {
	$txtButton = 'Lancer la planification';
	$actionButton = 'startPlanification';
	$titre = 'Dernière émission : ' . $type;
} else {
	$txtButton = 'Lancer l\'émission';
	$actionButton = 'startConduite';
	$titre = 'Prochaine émission : ' . $type;
}

switch ($type) {

	case 'inter-saison':
		$titre = 'Aucune saison planifiée';
		$txtButton = 'Lancer le casting';
		break;

	case 'casting':
		if (!EM_isDerniereTerminee()) {
			$titre = 'Casting en cours. ' . EM_getNbrCandidatsEnLice() . ' inscrits.';
			$txtButton = 'Cloturer le casting';
			if (EM_getNbrCandidatsEnLice() == 0) $actionButton = '" disabled="';
			else $actionButton = 'startConduite';
		} else $titre = 'Dernière émission : ' . $type;
		break;
}

$retour['contenuHTML']=preg_replace('/{#TITRE}/', $titre, $retour['contenuHTML']);
$retour['contenuHTML']=preg_replace('/{#TXTBUTTON}/', $txtButton, $retour['contenuHTML']);
$retour['contenuHTML']=preg_replace('/{#TYPEACTION}/', $actionButton, $retour['contenuHTML']);

return $retour;