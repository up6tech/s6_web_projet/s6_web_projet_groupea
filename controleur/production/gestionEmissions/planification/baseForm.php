<?php

/**
 * Permet de générer le formulaire de base pour toutes les émissions
 * @param string $typeEmission
 * @return array
 */
function genBaseForm(string $typeEmission): array {
	$retour = array();

	// On remplace le pattern par la date de demain (on ne planifie pas une émission pour le jour même)
	$retour['contenuHTML'] = preg_replace('/{#TODAY}/',
		date('Y-m-d', time() + (1 * 24 * 3600)), //
		file_get_contents("vue/html/production/gestionEmissions/planification/baseForm.html"));

	if ($typeEmission == 'casting') {
		$retour['titreHTML'] = 'Lancement du casting';
		$retour['contenuHTML'] = preg_replace('/{#TITRE}/', 'Lancer le casting', $retour['contenuHTML']);
	}
	else {
		$retour['titreHTML'] = 'Planification ' . $typeEmission;
		$retour['contenuHTML'] = preg_replace('/{#TITRE}/', 'Planifier ' . $typeEmission, $retour['contenuHTML']);
	}

	return $retour;
}