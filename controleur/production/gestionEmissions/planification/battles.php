<?php

require_once('controleur/production/gestionEmissions/planification/baseForm.php');

require_once('modele/bdd/candidat.php');

// Si on demande une page simple
if (!isset($_POST['date']) || !isset($_POST['heure'])) {

	$retour = genBaseForm('les battles');

	require_once('modele/bdd/utilisateur.php');

	// On récupère le patron html pour générer le formulaire
	$htmlPropre = file_get_contents('vue/html/production/gestionEmissions/planification/battles.html');
	$formulaire = '';

	for ($equipe = 1; $equipe <= 4; $equipe++) { // On passe toutes les équipes

		$formulaire .= "<br><h3>Equipe $equipe</h3>";
		// On récupère les qualifiés
		$qualifesEquipe = CAND_getQualifies(EM_getIdDerniereEmission(), $equipe);

		// On constitue les battles
		for ($noQualifie = 0; $noQualifie < count($qualifesEquipe); $noQualifie+=2) {

			$formulaire .= $htmlPropre; // On ajoute le patron HTML
			$noms = '';

			if (count($qualifesEquipe) - $noQualifie == 3) $nombreTours = 3; // Le nombre de candidats dans cette équipe est impair on prévois un trio (dernières battle)
			else $nombreTours = 2; // On est sur duo classique

			for ($j = 0; $j < $nombreTours; $j++) {
				$user = USER_getInfosById($qualifesEquipe[$noQualifie + $j]);
				$noms .= '<h5>'.$user['prenom'].' '.$user['nom'].'</h5>';
			}

			$formulaire = preg_replace('/{#NOMS}/', $noms, $formulaire);
			$formulaire = preg_replace('/{#NUMBATTLE}/', $equipe.'-'.$noQualifie/2, $formulaire);

			if (count($qualifesEquipe) - $noQualifie == 3) $noQualifie++;
		}
	}
	$retour['contenuHTML'] = preg_replace('/<!--SUITE-->/', $formulaire, $retour['contenuHTML']);

	return $retour;
} else { // On répond au formulaire

	if (EM_creerEmission('battles', $_POST['date'], $_POST['heure'])) {

		$ordrePassage = 0;

		for ($equipe = 1; $equipe <= 4; $equipe++) { // On passe toutes les équipes

			// On récupère les qualifiés
			$qualifesEquipe = CAND_getQualifies(EM_getIdAvantDerniereEmission(), $equipe);

			// On constitue les battles
			for ($noQualifie = 0; $noQualifie < count($qualifesEquipe); $noQualifie+=2) {

				if (count($qualifesEquipe) - $noQualifie == 3) $nombreTours = 3; // Le nombre de candidats dans cette équipe est impair on prévois un trio (dernières battle)
				else $nombreTours = 2; // On est sur duo classique

				for ($j = 0; $j < $nombreTours; $j++) {
					var_dump("salut");
					CAND_insertionChanter($qualifesEquipe[$noQualifie + $j], $_POST['artiste'.$equipe.'-'.$noQualifie/2], $_POST['titre'.$equipe.'-'.$noQualifie/2], null, $ordrePassage);
				}

				if (count($qualifesEquipe) - $noQualifie == 3) $noQualifie++;
				$ordrePassage ++;
			}
		}
	}
	header('Location: /');
}