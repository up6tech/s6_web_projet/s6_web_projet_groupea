<?php

require_once('controleur/production/gestionEmissions/planification/baseForm.php');

// Si on demande une page simple
if (!isset($_POST['date']) || !isset($_POST['heure'])) {
	return genBaseForm('casting');

} else { // On viens de répondre au formulaire

	if (!EM_creerEmission('casting', $_POST['date'], $_POST['heure'])) {

		$retour = genBaseForm('casting');
		require_once("modele/utils/alertManager.php");
		$retour['contenuHTML'] = ALERT_show_s($retour['contenuHTML'], 'Erreur de création de l\'émission', 'danger');
		return $retour;

	} else {
		header("Location: /"); // On a fini on redirige
		die();
	}
}



