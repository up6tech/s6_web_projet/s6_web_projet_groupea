<?php

require_once('controleur/production/gestionEmissions/planification/baseForm.php');
require_once('modele/bdd/candidat.php');

// Si on demande une page simple
if (!isset($_POST['date']) || !isset($_POST['heure'])) {

	$retour = genBaseForm('les auditions');

	require_once('modele/bdd/utilisateur.php');
	// On récupère les qualifiés.
	$qualifes = CAND_getQualifies(EM_getIdDerniereEmission());


	// On récupère le patron html pour générer le formulaire
	$htmlPropre = file_get_contents('vue/html/production/gestionEmissions/planification/casting.html');
	$formulaire = '';
	$i = 1;

	foreach ($qualifes as $idCandidat) {
		$formulaire .= $htmlPropre;
		$candidat = USER_getInfosById($idCandidat);

		$formulaire = preg_replace('/{#NOMCANDIDAT}/', $i.' - '.$candidat['prenom'].' '.$candidat['nom'], $formulaire);
		$formulaire = preg_replace('/{#IDCANDIDAT}/', $idCandidat, $formulaire);
		$i++;
	}
	$retour['contenuHTML']=preg_replace('/<!--SUITE-->/', $formulaire, $retour['contenuHTML']);

	return $retour;

} else { // On viens de répondre au formulaire

	if (!EM_creerEmission('auditions', $_POST['date'], $_POST['heure'])) {

		$retour = genBaseForm('les auditions');
		require_once("modele/utils/alertManager.php");
		$retour['contenuHTML'] = ALERT_show_s($retour['contenuHTML'], 'Erreur de création de l\'émission', 'danger');
		return $retour;

	} else {

		$qualifes = CAND_getQualifies(EM_getIdAvantDerniereEmission());
		shuffle($qualifes); // On mélange pour avoir un ordre aléatoire.
		$i = 0;
		foreach ($qualifes as $idCandidat) {
			CAND_insertionChanter($idCandidat, $_POST['artiste'.$idCandidat], $_POST['titre'.$idCandidat], null, $i);
			$i++;
		}

		header("Location: /"); // On a fini on redirige
	}
}



