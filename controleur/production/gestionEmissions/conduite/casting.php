<?php

require_once("modele/bdd/emission.php");
require_once("modele/bdd/candidat.php");

if (count($_POST) == 1) { // On demande simplement liste du casting

	$retour = array();
	$html = require_once("vue/html/production/gestionEmissions/conduite/casting.php");
	require_once("modele/bdd/utilisateur.php");
	require_once('modele/utils/youtubeParser.php');

	// Récupération des prestations des candidats.
	$prestations = CAND_getChanter(EM_getIdDerniereEmission());
	$champsHTML = ''; // On va stocker le HTML généré pour le formulaire
	foreach ($prestations as $presta ) {
		$champsHTML .= $html['champ'];
		$champsHTML=preg_replace('/{#YOUTUBE}/', YT_getHTML($presta['lienYtb']), $champsHTML);
		$chanteur = USER_getInfosById($presta['idUtilisateur']);
		$champsHTML=preg_replace('/{#NOM}/', $chanteur['prenom'].' '.$chanteur['nom'], $champsHTML);
		$champsHTML=preg_replace('/{#CHANSON}/', $presta['chanson'], $champsHTML);
		$champsHTML=preg_replace('/{#ID}/', $presta['idUtilisateur'], $champsHTML);
	}

	$retour['contenuHTML'] = preg_replace('/{#CHAMPS}/', $champsHTML, $html['corp']);
	return $retour;

} else { // On viens de répondre au casting

	$prestations = CAND_getChanter(EM_getIdDerniereEmission()); // On récupère toutes les prestations du casting
	$idEmission = EM_getIdDerniereEmission();

	//On balaye les prestations
	foreach ($prestations as $presta) {
		// Si l'id est dans $_POST c'est que la prestation à été retenue
		CAND_setResultat($idEmission, $presta['idUtilisateur'], in_array($presta['idUtilisateur'], $_POST));
	}
	header('Location: /');
}
