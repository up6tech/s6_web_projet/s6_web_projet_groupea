<?php

require_once('modele/bdd/candidat.php');
require_once('modele/bdd/utilisateur.php');
require_once('modele/utils/youtubeParser.php');

$presta = CAND_conduiteGetSuivant();

$retour = array();
$retour['titreHTML'] = "Conduite des Battles";
$retour['contenuHTML'] = file_get_contents('vue/html/production/gestionEmissions/conduite/battles.html');

// On a répondu au formulaire donc on enregistre
if (isset($_POST['gagnant']) && isset($_POST['lienYtb'])) {

	for ($i = 0; $i < count($presta); $i++) {
		if ($i == $_POST['gagnant']) {
			CAND_editChanter($presta[$i]['idUtilisateur'], 'lienYtb', YT_getID($_POST['lienYtb']));
			$nbrPoints = 1;
		} else $nbrPoints = -1;
		CAND_editChanter($presta[$i]['idUtilisateur'], 'pointsCoachs', $nbrPoints);
	}


	// On récupère la presta suivante pour la génération de la page
	$presta = CAND_conduiteGetSuivant();
	if (!$presta) { // Si elle est nulle c'est qu'on a fini
		$retour['contenuHTML'] = '<h1>Emission terminée</h1><br><div class="container text-center"><a class="btn btn-primary" href="/">Revenir à l\'acceuil</a></div>';
		return $retour;
	}
}

// On remplis le patron HTML
$retour['contenuHTML']=preg_replace('/{#NUMEQUIPE}/', CAND_getEquipe($presta[0]['idUtilisateur']), $retour['contenuHTML']);
$retour['contenuHTML']=preg_replace('/{#CHANSON}/', $presta[0]['chanson'], $retour['contenuHTML']);

for ($i = 0; $i < count($presta); $i++) {
	$retour['contenuHTML']=preg_replace('/{#CANDIDAT'.$i.'}/', $presta[$i]['prenom'].' '.$presta[$i]['nom'], $retour['contenuHTML']);
}

if (count($presta) == 3) $retour['contenuHTML']=preg_replace('/ hidden>/', '>', $retour['contenuHTML']);

return $retour;