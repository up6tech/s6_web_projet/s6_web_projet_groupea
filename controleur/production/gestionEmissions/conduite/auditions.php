<?php

require_once('modele/bdd/candidat.php');
require_once('modele/utils/youtubeParser.php');

$presta = CAND_conduiteGetSuivant();

$retour = array();
$retour['titreHTML'] = "Conduite des auditions";
$retour['contenuHTML'] = file_get_contents('vue/html/production/gestionEmissions/conduite/auditions.html');

// On a répondu au formulaire donc on enregistre
if (isset($_POST['equipe']) && in_array($_POST['equipe'], array(0,1,2,3,4)) && isset($_POST['lienYtb'])) {

	if ($_POST['equipe'] == 0) $nbrPoints = -1; // Il est disqualifié
	else { // On continue l'aventure
		CAND_setEquipe($presta['idUtilisateur'], $_POST['equipe']);
		$nbrPoints = 1;
	}

	CAND_editChanter($presta['idUtilisateur'], 'lienYtb', YT_getID($_POST['lienYtb']));
	CAND_editChanter($presta['idUtilisateur'], 'pointsCoachs', $nbrPoints);

	// On récupère la presta suivante pour la génération de la page
	$presta = CAND_conduiteGetSuivant();
	if (!$presta) { // Si elle est nulle c'est qu'on a fini
		$retour['contenuHTML'] = '<h1>Emission terminée</h1><br><div class="container text-center"><a class="btn btn-primary" href="/">Revenir à l\'acceuil</a></div>';
		return $retour;
	}
}

// On remplis le patron HTML
$retour['contenuHTML']=preg_replace('/{#NOM}/', $presta['prenom'].' '.$presta['nom'], $retour['contenuHTML']);
$retour['contenuHTML']=preg_replace('/{#CHANSON}/', $presta['chanson'], $retour['contenuHTML']);

return $retour;