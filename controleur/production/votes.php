<?php

// on utilise un cookie afin de conserver le numéro d'équipe choisi lorsque que le code est réexécuté
if (EM_getTypeDerniereEmission()=="demi-finale") {
    if (isset($_COOKIE['Equipe'])) {
        $Equipe = $_COOKIE['Equipe'];                       // récupérer l'ancienne valeur
        setcookie('Equipe', $Equipe, time()+60);            // modifier le cookie
    }
    else {
        setcookie('Equipe', $_POST['Equipe'], time()+60);
    }
}

// inclusion des fichiers pour les fonctions utilisées
require_once("modele/bdd/emission.php");
require_once("modele/bdd/production.php");

// preparation du $retour pour constituer la page HTML
$retour = array();
$retour['titreHTML'] = "Votes public";

// variables
$idCandidats = array();
$notes  = array();
$nomHTML = array();
$chansonHTML = array();
$noteTelespect = array();
$notesFinales = array();
$points = array();
$scoresFinaux = array();
$nbCandidats = 4;

// si on n'est ni en finale ni en demi-finale (ou que si on y était, l'émission est terminée)
if (EM_getTypeDerniereEmission()!="demi-finale" && EM_getTypeDerniereEmission()!="finale" || EM_isDerniereTerminee()) {
    // on retourne la page vote_impossible.html
    $retour['contenuHTML'] = file_get_contents("vue/html/production/votes/vote_impossible.html");
    return $retour;
}

// sinon, si on est en demi-finale et qu'on n'a pas encore choisi d'équipe pour laquelle on veut créer un vote
else if (EM_getTypeDerniereEmission()=="demi-finale" && !isset($_COOKIE['Equipe'])) {
    // on retourne la page votes_choixEquipe.html
    $retour['contenuHTML'] = file_get_contents("vue/html/production/votes/votes_choixEquipe.html");
    return $retour;
}

// sinon, ça veut dire qu'on est en demi-finale et qu'on a choisi une équipe pour le vote, ou bien qu'on est en finale
else {
    // donc on récupère les id des candidats pour chaque cas (demi-finale et finale)
    // d'abord pour la demi-finale (on récupère les id des candidats toujours en course et faisant partie de l'équipe choisie)
    if (EM_getTypeDerniereEmission()=="demi-finale") {
        $retour['contenuHTML'] = file_get_contents("vue/html/production/votes/votes_demiFinale.html");
        $h1HTML = 'Demi-Finale';
        for ($i=0; $i<$nbCandidats; $i++) {
            $idCandidats[$i] = PROD_getIdDemiFinalistes($_COOKIE['Equipe'])[$i];
        }
    }
    // et sinon pour la finale (on récupère simplement les id des candidats toujours en course)
    else {
        $retour['contenuHTML'] = file_get_contents("vue/html/production/votes/votes_finale.html");
        $h1HTML = 'Finale';
        for ($i=0; $i<$nbCandidats; $i++) {
            $idCandidats[$i] = PROD_getIdFinalistes()[$i];
        }
    }

    // ON RANGE LES ID PAR ORDRE CROISSANT !!!
    // Cela permet d'avoir le bon nom associé aux bons points au moment de l'affichage et de l'insertion dans la BDD
    sort($idCandidats);

    // grâce aux id, on peut récupérer les prénoms et noms des candidats pour lesquels on souhaite créer le vote
    // on pourra comme ça les afficher dans la page HTML
    for ($i=0; $i<$nbCandidats; $i++) {
        // on récupère
        $nomHTML[$i] = PROD_getPrenomNomById($idCandidats[$i]);
        $chansonHTML[$i] = PROD_getChansonByIdAndEmission($idCandidats[$i], EM_getIdDerniereEmission());
    }

    // on effectue l'insertion dans la page HTML des parties "universelles"
    $retour['contenuHTML']=preg_replace('/{#H1}/', $h1HTML, $retour['contenuHTML']);
    for ($i=0; $i<$nbCandidats; $i++) {
        $retour['contenuHTML']=preg_replace('/{#NOM'.($i+1).'}/', $nomHTML[$i], $retour['contenuHTML']);
        $retour['contenuHTML']=preg_replace('/{#CHANSON'.($i+1).'}/', $chansonHTML[$i], $retour['contenuHTML']);
    }

    // si on est en demi-finale ou finale et que les candidats pour lesquels on créée le vote ont déjà des points attribués
    if (PROD_recupererScores($idCandidats) != null) {
        
        // on affiche les notes finales dans la page HTML
        $messageHTML = '';
        $hidden = 'hidden';
        for ($i=0; $i<$nbCandidats; $i++) {
            $scoresFinaux[$i] = PROD_recupererScores($idCandidats)[$i];
        }

        $retour['contenuHTML']=preg_replace('/{#HIDDEN}/', $hidden, $retour['contenuHTML']);
        for ($i=0; $i<$nbCandidats; $i++) {
            $retour['contenuHTML']=preg_replace('/{#POINTS'.($i+1).'}/', $scoresFinaux[$i], $retour['contenuHTML']);
        }

        $retour['contenuHTML']=preg_replace('/{#TOTAL}/', array_sum($scoresFinaux), $retour['contenuHTML']);
        $retour['contenuHTML']=preg_replace('/{#MESSAGE}/', $messageHTML, $retour['contenuHTML']);

        return $retour;
    }

    // si on arrive ici, c'est que les candidats n'ont pas encore de points attribués
    // donc si le vote n'existe pas déjà, on le créée
    if (!PROD_isVoteExistant()) {
        PROD_creerVote($idCandidats);
    }


    // UNE FOIS QUE LES TELESPECTATEURS ONT VOTE

    // on calcule les scores obtenus grâce aux telespectateurs
    // si des téléspectateurs ont participé au vote
    if (PROD_recupererNbTotalVotes() != null) {
        // on calcule le nombre de votes en tout
        $nbVotesTotal = PROD_recupererNbTotalVotes();
        // pour chaque candidat, s'il n'a reçu aucun vote, on lui met 0
        for ($i=0; $i <$nbCandidats; $i++) {
            if (PROD_recupererVotesTelespect()[$i][0] == null) {
                $nbVotesCandidat = 0;
            }
            // sinon, on récupère le nombre de votes qu'il a obtenus
            else {
                $nbVotesCandidat = PROD_recupererVotesTelespect()[$i][0];
            }
            // et on calcule le pourcentage que ça donne
            $noteTelespect[$i] = round($nbVotesCandidat / $nbVotesTotal * 100);
        }
    }
    // sinon, si aucun téléspectateur n'a participé au vote, on met 0 à tout le monde
    else {
        for ($i=0; $i <$nbCandidats; $i++) {
            $noteTelespect[$i] = 0;
        }
    }

    // ensuite, si on est en demi-finale
    if (EM_getTypeDerniereEmission()=="demi-finale") {

        // ...si les notes ne sont pas remplies ou que le total ne fait pas 50
        if (!isset($_POST['note1']) ||
            !isset($_POST['note2']) ||
            !isset($_POST['note3']) ||
            !isset($_POST['note4']) ||
            intval($_POST['note1']) + intval($_POST['note2']) + intval($_POST['note3']) + intval($_POST['note4']) != 50) {
            
            // on prépare l'affichage et on retourne la page
            $messageHTML = '(Le total ne fait pas 50 !)';
            for ($i=0; $i<$nbCandidats; $i++) {
                $retour['contenuHTML']=preg_replace('/{#POINTS'.($i+1).'}/', '', $retour['contenuHTML']);
            }
            $retour['contenuHTML']=preg_replace('/{#TOTAL}/', 0, $retour['contenuHTML']);
            $retour['contenuHTML']=preg_replace('/{#MESSAGE}/', $messageHTML, $retour['contenuHTML']);
            return $retour;
        }

        // ...sinon (=si les notes sont remplies et que le total fait 50)
        else {
            // on modifie l'affichage
            $messageHTML = '';
            $hidden = 'hidden';

            // on insère les notes finales (=somme des notes du jury et des téléspectateurs) dans la BDD
            $points = array(intval($_POST['note1']),
                            intval($_POST['note2']),
                            intval($_POST['note3']),
                            intval($_POST['note4']));
            for ($i=0; $i <$nbCandidats; $i++) {
                $notesFinales[$i]= $points[$i] + $noteTelespect[$i];
            }
            PROD_insererNotes($notesFinales, $idCandidats);

            // on va chercher les notes dans la BDD, et on les affiche sur la page
            $retour['contenuHTML']=preg_replace('/{#HIDDEN}/', $hidden, $retour['contenuHTML']);
            for ($i=0; $i<$nbCandidats; $i++) {
                $retour['contenuHTML']=preg_replace('/{#POINTS'.($i+1).'}/', $notesFinales[$i], $retour['contenuHTML']);
            }
            $retour['contenuHTML']=preg_replace('/{#TOTAL}/', array_sum($notesFinales), $retour['contenuHTML']);
            $retour['contenuHTML']=preg_replace('/{#MESSAGE}/', $messageHTML, $retour['contenuHTML']);
        }
        return $retour;
    }

    // sinon, si on est en finale
    else {
        // on affiche les résultats actuels des votes des téléspectateurs
        for ($i=0 ; $i<$nbCandidats; $i++) {
            if ($noteTelespect[$i] == null ) {
                $noteTelespect[$i] = 0;
            }
        }
        for ($i=0; $i<$nbCandidats; $i++) {
            $retour['contenuHTML']=preg_replace('/{#POINTS'.($i+1).'}/', $noteTelespect[$i], $retour['contenuHTML']);
        }
        $retour['contenuHTML']=preg_replace('/{#TOTAL}/', array_sum($noteTelespect), $retour['contenuHTML']);

        // si la case "Fermer le vote" a été cochée et validée
        if (isset($_POST['fermerVote'])) {
            // on insère les notes dans la BDD
            PROD_insererNotes($noteTelespect, $idCandidats);
        }
    }
    return $retour;
}