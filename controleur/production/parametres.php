<?php

if (isset($_POST['password']) && isset($_POST['confPassword'])) {
    if ($_POST['password'] == $_POST['confPassword']) {
        require_once('modele/bdd/production.php');
        PROD_edit('password', USER_hashPassword($_POST['password']));
    }
}

$retour = array();
$retour['contenuHTML'] = file_get_contents('vue/html/production/parametres.html');

$retour['titreHTML'] = "Paramètres du compte";

return $retour;
