<?php

/**
 * Le but de ce fichier est de faire aiguillage vers le contrôleur corespondant à la phase du concours actuellement en cours.
 */

require_once('modele/bdd/emission.php');

$path = 'controleur/production/gestionEmissions/';

// CAS PARTICULIER DU CASTING
if (EM_getTypeDerniereEmission() == 'casting') {
	if (isset($_POST['startConduite'])) return require_once($path.'conduite/casting.php');
	elseif (!isset($_POST['startPlanification'])) return require_once($path.'attente.php');
}

if (EM_isEmissionEnCours()) $path .= 'conduite/'; // L'émission est en cours on veut la gérer.
elseif (EM_isPlanificationEnCours()) $path .= 'planification/'; // On est en cours de planification.
else return require_once($path.'attente.php');

$type = EM_getTypeDerniereEmission();
if (isset($_POST['startPlanification']))
	$type = EM_getTypeProchaineEmission($type);

return require_once($path.$type.'.php');