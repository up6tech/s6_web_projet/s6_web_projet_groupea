<?php

// Envoi de la page simple
if (   !isset($_POST['prenom'])
    || !isset($_POST['nom'])
    || !isset($_POST['dateNaissance'])
    || !isset($_POST['sexe'])
    || !isset($_POST['tel'])
    || !isset($_POST['mail'])
    || !isset($_POST['adresse'])
    || !isset($_POST['codePostal'])
    || !isset($_POST['ville'])
    || !isset($_POST['password'])
    || !isset($_POST['confPassword'])
    || !isset($_POST['questionSecrete'])
    || !isset($_POST['repQuestionSecrete'])
    || !filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)
	|| strlen($_POST['mail']) > 100
	|| !preg_match('/([0-9]){10}/', $_POST['tel'])
    || !preg_match('/([0-9]){1,4}-([0-9]){1,2}-([0-9]){1,2}/', $_POST['dateNaissance'])
    || strlen($_POST['sexe']) != 1)

    return include("vue/html/inscription.php");

else { // Le client viens de répondre au formulaire donc on passe à l'inscritption.

	require_once('modele/bdd/production.php');
	if ($_POST['mail'] == PROD_getMail()) {
		require_once('modele/utils/alertManager.php');
		return ALERT_show('vue/html/inscription.php', 'Echec de la création du compte, Mail déja utilisé.', 'danger');
	}

    require_once("modele/bdd/utilisateur.php");

    $civilite = "";
    switch ($_POST['sexe']) {
        case 'H':
            $civilite = "M";
            break;
        case 'F':
            $civilite = "Mme";
            break;
        default:
            break;
    }

    // On vérifie que la date de naissance soit passée.
    $dateN = date_create_from_format('Y-m-d', $_POST['dateNaissance']);
    $now = date_create();

    if ($dateN >= $now && $_POST['password'] == $_POST['confPassword']) { // Si la date de naissance est dans le futur
	    require_once('modele/utils/alertManager.php');
	    return ALERT_show('vue/html/inscription.php', 'Données du formulaire invalides. Date dans le futur ou confirmation du mot de passe incorrecte.', 'danger');
    } else {

    	// On évite de dépasser la taille d'un champ dans la BDD
	    $_POST['prenom'] = substr($_POST['prenom'], 0, 50);
	    $_POST['nom'] = substr($_POST['nom'], 0, 50);
	    $_POST['adresse'] = substr($_POST['adresse'], 0, 125);
	    $_POST['codePostal'] = substr($_POST['codePostal'], 0, 7);
	    $_POST['ville'] = substr($_POST['ville'], 0, 66);
	    $_POST['questionSecrete'] = substr($_POST['questionSecrete'], 0, 200);
	    $_POST['repQuestionSecrete'] = substr($_POST['repQuestionSecrete'], 0, 200);

    	$adresse = $_POST['adresse'] . " " . $_POST['codePostal'] . " " . $_POST['ville'];
	    if (USER_createAccount($_POST['prenom'], $_POST['nom'], $civilite,
		    $_POST['tel'], $_POST['mail'], $adresse,
		    $_POST['password'], $_POST['dateNaissance'], $_POST['questionSecrete'],
		    $_POST['repQuestionSecrete'])) {

		    $idCompte = USER_getIdByMail($_POST['mail']);
		    $_SESSION['idUtilisateur'] = $idCompte;
		    $_SESSION['prenom'] = $_POST['prenom'];
		    $_SESSION['typeCompte'] = "télespectateur";
		    header("Location: /");  // Redirection vers la page d'accueil.
	    } else

		    require_once('modele/utils/alertManager.php');
	        return ALERT_show('vue/html/inscription.php', 'Echec de la création du compte', 'danger');
    }
}