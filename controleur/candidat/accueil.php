<?php
require_once('modele/bdd/emission.php');
require_once ('modele/bdd/candidat.php');
require_once("modele/utils/youtubeParser.php");

$retour = array();
$retour['contenuHTML'] = '<h1>La Nouvelle Voix</h1><br>';

$qualifies = CAND_getQualifies(EM_getIdDerniereEmission());
if ($qualifies != null){
    foreach ($qualifies as $idCandidat){
        if ($idCandidat == $_SESSION['idUtilisateur']){
            $retour['contenuHTML'] .= '<h2>Félicitations, vous êtes parmi les '.EM_getNbrCandidatsEnLice().' candidats qualifiés!</h2><br><br>';
        }
    }
}

$retour['contenuHTML'] .= '<h2>La prochaine émission : '.strtoupper(EM_getTypeProchaineEmission()).'</h2>';


$retour['contenuHTML'] .= '<br><h2>Vos dernières performances</h2><br>';
$infos = CAND_getInfosPerformances($_SESSION['idUtilisateur']);
$nbVideos = 0;
if ($infos != null) {
    $retour['contenuHTML'] .= '<div class="container">';
    foreach ($infos as $performance) {
    	if ($performance['lienYtb']) {
		    if ($nbVideos % 3 == 0) {
			    if ($nbVideos != 0) $retour['contenuHTML'] .= '</div>';
			    $retour['contenuHTML'] .= '<div class="row">';
		    }
		    $retour['contenuHTML'] .= '<div class="col"><h3>' . strtoupper($performance['type']) . '</h3>' . YT_getHTML($performance['lienYtb']) . '</div>';
		    $nbVideos++;
	    }
    }
    $retour['contenuHTML'] .= '</div></div>';
}else{
    $retour['contenuHTML'] .= '<h3>Aucune performance à afficher</h3>';
}

return $retour;
