# UPSSITECH S6 Projet WebDynamique Groupe 1-A

Déploiement automatique

[![pipeline status](https://gitlab.com/up6tech/s6_web_projet/s6_web_projet_groupea/badges/master/pipeline.svg)](https://gitlab.com/up6tech/s6_web_projet/s6_web_projet_groupea/-/commits/master)

![Logo LaNouvelleVoix](https://gitlab.com/up6tech/s6_web_projet/s6_web_projet_groupea/-/raw/master/vue/css/logo.png)

- Compte production :
    - production[at]lanouvellevoix.fr
    - password
- Compte candidat :
    - dorian[at]doriangardes.fr
    - password
    
Ce projet n'est pas terminé il reste donc certains bugs et fonctionalités non implémentées.
