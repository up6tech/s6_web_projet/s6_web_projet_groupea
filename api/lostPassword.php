<?php

require_once('../modele/bdd.php');
require_once('../modele/bdd/utilisateur.php');

if (isset($_POST['type'])) {

	require_once('../modele/utils/cleanInput.php');
	$_POST = cleanArray($_POST);
	header('Content-Type: application/json');

	switch ($_POST['type']) {

		case "getQuestionSecrete":
			if (isset($_POST['mail'])) {
				if ($compte = USER_getInfosByMail($_POST['mail'])) {
					echo json_encode(array($compte['prenom'] . " " . $compte['nom'], $compte['questSecrete']));
				} else echo json_encode(null);;
			}
			break;
		case "checkReponseSecrete": // TODO Il faudrait mettre en place un système anti brute-force !!!!!
			if (isset($_POST['mail']) && isset($_POST['reponse'])) {
				$compte = USER_getInfosByMail($_POST['mail']);
				if ($compte != null && $compte['repSecrete'] == $_POST['reponse']) {
					echo json_encode("OK");
				} else echo json_encode(null);;
			}
			break;
		default:
			echo "{}";
			break;
	}
} else echo "";